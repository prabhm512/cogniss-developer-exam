const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "input2.json";
const outputFile = "output2.json";

let emails = [];
let output = {};

jsonfile.readFile(inputFile, (err, body) => {
    if (err) {
        console.log(err);
    }
    // Spread response body into emails array
    emails = [...body.names];
    
    // Split names, generate random characters and append that to the names along with '@gmail.com'
    for (let i=0; i<emails.length; i++) {
        const reversedName = emails[i].split("").reverse().join("");
        const randomChars = randomstring.generate(5);
        const randomCharsAppended = reversedName + randomChars + "@gmail.com";

        emails.splice(i, 1, randomCharsAppended);
    }

    output = {
        "emails": [...emails]
    }

    // Write output to output2.json
    jsonfile.writeFile(outputFile, output, {spaces: 2}, (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("Emails created!");
        }
    })
});
